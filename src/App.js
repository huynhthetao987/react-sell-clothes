import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Header from './Components/Layout/Header';
import Slider from './Components/Layout/Slider';
import MenuLeft from './Components/Layout/MenuLeft';
import Footer from './Components/Layout/Footer';

class App extends Component {
  constructor(props){
    super(props)
  }
  render () {
    let pathname = this.props.location.pathname;
    return (
      <div>
        <Header />
        {pathname.includes('home') || pathname.includes('') ? <Slider /> : ""}
        <section>
          <div class="container">
            <div class="row">
              {pathname.includes('account') || pathname.includes('cart') ? "" : <MenuLeft />}
              {this.props.children}
            </div>
          </div>
        </section>              
        <Footer />
      </div>
    )
  }
}

export default withRouter(App);
