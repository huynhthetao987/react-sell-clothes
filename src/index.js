import React from 'react';
import ReactDOM from 'react-dom';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

import './index.css';
import App from './App';
import Home from './Components/Home';
import Blog from './Components/Blog/Index';
import Detail from './Components/Blog/Detail';
import Comment from './Components/Blog/Comment';
import Rate from './Components/Blog/Rate';
// import Register from './Components/Member/Register';
import Login from './Components/Member/Login';
import Account from './Components/Account/Index';
import Cart from './Components/Product/Cart';
import reportWebVitals from './reportWebVitals';
import ProductDetail from './Components/Product/ProductDetail';

ReactDOM.render(
    <Router>
        <App>
            <Switch>
                <Route exact path='/'  component={Home} />
                <Route path='/home' component={Home} />
                <Route path='/blog/list' component={Blog} />
                <Route path='/blog/detail/:id' component={Detail} />
                <Route path='/blog/comment/:id' component={Comment} />
                <Route path='/blog/rate/:id' component={Rate} />
                {/* <Route path='/register' component={Register} /> */}
                <Route path='/login' component={Login} />
                <Route path='/product/cart' component={Cart} />
                <Route path='/product/detail/:id' component={ProductDetail} />
                <Route component={Account} />
            </Switch>
        </App>

    </Router>,
     document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
