import React, { Component } from 'react';
import Axios from 'axios';

class ProductDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            product: '',
            images: [],
            id_user: '',
            srcSmall: ''
        }
        this.handleClickImg = this.handleClickImg.bind(this);
    }

    componentDidMount() {
        var id = this.props.match.params.id;
        Axios.get('http://localhost/laravel/laravel/public/api/product/detail/' + id)
        .then(res => {
            const product = res.data.data;
            const id_user = product.id_user;
            const images = JSON.parse(product.image);
            this.setState({product,id_user, images})
        })
    }

    fetchDataImage() {
        let images = this.state.images;
        return images.map((value, key) => {
            return (
                <a href><img src={'http://localhost/laravel/laravel/public/upload/user/product/' + this.state.id_user + '/' + value} alt="" onClick={this.handleClickImg} /></a>
            )
        })
    }

    handleClickImg(e) {
        const srcSmall = e.target.src;
        this.setState({srcSmall})
    }

    render() {
        let product = this.state.product;
        let images = this.state.images;
        let srcSmall = this.state.srcSmall;
        console.log(srcSmall)
        const srcLarge = srcSmall ? srcSmall : 'http://localhost/laravel/laravel/public/upload/user/product/' + this.state.id_user + '/' + images[0]; 
        return( 
            <div className="col-sm-9 padding-right">
                <div className="product-details">
                    <div className="col-sm-5">
                        <div className="view-product">
                            <img src={srcLarge} />
                            <a href="images/product-details/1.jpg" rel="prettyPhoto"><h3>ZOOM</h3></a>
                        </div>
                        <div id="similar-product" className="carousel slide" data-ride="carousel">
                            <div className="carousel-inner">
                                <div className="item active">
                                    {this.fetchDataImage()}
                                </div>
                            </div>
                            <a className="left item-control" href="#similar-product" data-slide="prev">
                                <i className="fa fa-angle-left" />
                            </a>
                            <a className="right item-control" href="#similar-product" data-slide="next">
                                <i className="fa fa-angle-right" />
                            </a>
                        </div>
                    </div>
                    <div className="col-sm-7">
                        <div className="product-information">
                            <img src="images/product-details/new.jpg" className="newarrival" alt="" />
                            <h2>{product.name}</h2>
                            <p>Web ID: 1089772</p>
                            <img src="images/product-details/rating.png" alt="" />
                            <span>
                                <span>{'US $' + product.price}</span>
                                <label>Quantity:</label>
                                <input type="text" defaultValue={3} />
                                <button type="button" className="btn btn-fefault cart">
                                <i className="fa fa-shopping-cart" />
                                Add to cart
                                </button>
                            </span>
                            <p><b>Availability:</b> In Stock</p>
                            <p><b>Condition:</b> New</p>
                            <p><b>Brand:</b> E-SHOPPER</p>
                            <a href><img src="images/product-details/share.png" className="share img-responsive" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ProductDetail