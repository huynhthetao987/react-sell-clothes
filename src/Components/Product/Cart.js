import React, { Component } from 'react';
import Axios from 'axios';
import HandleCart from './HandleCart';

class Cart extends Component {
    constructor(props) {
        super(props)
        this.state = {
            products: [],
            id_user: ''
        }
        this.deleteProduct = this.deleteProduct.bind(this);
    }

    componentDidMount() {
        const dataIdProduct = JSON.parse(localStorage.getItem('idProduct'));
        Axios.post('http://localhost/laravel/laravel/public/api/product/cart', dataIdProduct)
        .then(res => {
            this.setState({
                products: res.data.data
            })
        })
    }

    fetchData() {
        let products = this.state.products;
        return products.map((value, key) => {
            return (
                <HandleCart product = {value} deleteProduct = {this.deleteProduct} />    
            )
        })
    }

    deleteProduct(idProduct) {
        let products = this.state.products;
        products.map((value, key) => {
            if(value.id == idProduct) {
                delete products[key]
                this.setState({products})
            }
        })

        let dataIdProduct = JSON.parse(localStorage.getItem('idProduct'))
        Object.keys(dataIdProduct).map((key, index) => {
            if(idProduct == key) {
                delete dataIdProduct[idProduct];
            }
        })
        
        localStorage.setItem('idProduct', JSON.stringify(dataIdProduct))

    }

    render() {
        return(
            <section id="cart_items">
                <div className="container">
                    <div className="breadcrumbs">
                        <ol className="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li className="active">Shopping Cart</li>
                        </ol>
                    </div>
                    <div className="table-responsive cart_info">
                        <table className="table table-condensed">
                            <thead>
                                <tr className="cart_menu">
                                    <td className="image">Item</td>
                                    <td className="description" />
                                    <td className="price">Price</td>
                                    <td className="quantity">Quantity</td>
                                    <td className="total">Total</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                {this.fetchData()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </section> 
        )
    }
}

export default Cart
