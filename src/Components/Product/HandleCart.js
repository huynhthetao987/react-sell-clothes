import React, { Component } from 'react';

class HandleCart extends Component {
    constructor(props) {
        super(props)
        this.state = {
            product: {},
            price: '',
            qty: '',
            images: [],
            id_product: '',
            id_user: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount() {
        const product = this.props.product;
        let userData = JSON.parse(localStorage.getItem('userData'));
        let id_user = userData.auth.id;
        const images = JSON.parse(product.image)

        this.setState({
            product,
            price: product.price,
            qty: product.qty,
            images,
            id_product: product.id,
            id_user
        })
    }

    handleChange(e) {
        const nameDirection = e.target.name;
    
        if(nameDirection == 'up') {
            const qty = this.state.qty + 1;
            this.setState({qty})
        } else {
            if(this.state.qty > 1) {
                const qty = this.state.qty - 1;
                this.setState({qty})
            }
        }
    }

    handleDelete() {
        let id_product = this.state.id_product;
        this.props.deleteProduct(id_product)
    }

    render() {
        let product = this.state.product;
        let images = this.state.images;
        return(
            <tr>
                <td className="cart_product">
                    <a href><img src={'http://localhost/laravel/laravel/public/upload/user/product/' + this.state.id_user + '/' + images[0]} alt="" /></a>
                </td>
                <td className="cart_description">
                    <h4><a href>{product.name}</a></h4>
                    <p>{'Web ID: ' + product.id}</p>
                </td>
                <td className="cart_price">
                    <p>{'$' + this.state.price}</p>
                </td>
                <td className="cart_quantity">
                    <div className="cart_quantity_button">
                        <a className="cart_quantity_up" name='up' onClick={this.handleChange}> + </a>
                            <input className="cart_quantity_input" value={this.state.qty} type="text" name="quantity" autoComplete="off" size={2} />
                        <a className="cart_quantity_down" name='down' onClick={this.handleChange}> - </a>
                    </div>
                </td>
                <td className="cart_total">
                    <p className="cart_total_price">{'$' + this.state.price*this.state.qty}</p>
                </td>
                <td className="cart_delete">
                    <a className="cart_quantity_delete" onClick={this.handleDelete}><i className="fa fa-times" /></a>
                </td>
            </tr>        
        )
    }
}

export default HandleCart