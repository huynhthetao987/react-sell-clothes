import Axios from 'axios';
import React, { Component } from 'react';
import StarRatings from 'react-star-ratings';

class Rate extends Component {
    constructor(props) {
        super(props)
        this.state = {
            rating: 0,
            votes: '',
            errorRate: '',
            msg: ''
        }
        this.changeRating = this.changeRating.bind(this);
    }

    componentDidMount() {
        Axios.get('http://localhost/laravel/laravel/public/api/blog/rate/' + this.props.idBlog)
        .then(res => {
            const rates = res.data.data;
            let sum = 0;
            Object.keys(rates).map((key, index) => {
                sum = sum + rates[key]['rate'];
            })
            let size = Object.keys(rates).length;
            let medium = sum/size;
            this.setState({
                rating: medium,
                votes: size            })
        })
    }

    changeRating(newRating) {
        let userData = JSON.parse(localStorage.getItem('userData'));
        if(!userData) {
            this.setState({
                errorRate: 'dang nhap de danh gia'
            })
        } else {
            this.setState({
                rating: newRating
            })
            
            let accessToken = userData.token
            let config = {                
                headers: {                
                    'Authorization': 'Bearer ' + accessToken,             
                    'Content-Type': 'application/x-www-form-urlencoded',                
                    'Accept' : 'application/json'                
                } 
            }  
            const formData = new FormData();
                formData.append('user_id', userData.auth.id);
                formData.append('blog_id', this.props.idBlog);
                formData.append('rate', newRating);
            Axios.post('http://localhost/laravel/laravel/public/api/blog/rate/' + this.props.idBlog, formData, config)
            .then(res => {
                console.log(res)
                this.setState({
                    msg: res.data.message
                })
            })
        }
    }

    render() {
        return (
            <>  
                <p>{this.state.errorRate}</p>
                <p>{this.state.msg}</p>
                <div className="rating-area">
                    <ul className="ratings">
                        <li className="rate-this">Rate this item:</li>
                        <li>
                            <StarRatings
                                rating={this.state.rating}
                                starRatedColor="red"
                                changeRating={this.changeRating}
                                numberOfStars={5}
                                name='rating'
                            />
                        </li>
                        <li className="color">{'(' + this.state.votes + 'votes)'}</li>
                    </ul>
                    <ul className="tag">
                        <li>TAG:</li>
                        <li><a className="color" href>Pink <span>/</span></a></li>
                        <li><a className="color" href>T-Shirt <span>/</span></a></li>
                        <li><a className="color" href>Girls</a></li>
                    </ul>
                </div>
            </>
        )
    }
}

export default Rate