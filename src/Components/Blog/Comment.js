import React, { Component } from 'react';
import axios from 'axios';

class Comment extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: '',
            errorLogin: '',
            commentSuccess: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        this.setState ({
            value: e.target.value
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        let convertUserData = localStorage.getItem('userData')
        if (!convertUserData){
            this.setState({
              errorLogin: 'Please login to comment!'
            })
        } else {
            const userData = JSON.parse(convertUserData);
            let accessToken = userData.token;   
            let config = {                
                headers: {                
                    'Authorization': 'Bearer ' + accessToken,             
                    'Content-Type': 'application/x-www-form-urlencoded',                
                    'Accept': 'application/json'                
                } 
            }  
             
            let comment = this.state.value;      
            if(comment) {           
                const formData = new FormData();                
                formData.append('id_blog', this.props.idBlog);                
                formData.append('id_user', userData.auth.id);    
                formData.append('name_user', userData.auth.name);             
                formData.append('id_comment', this.props.getIdReplay ? this.props.getIdReplay : 0);                
                formData.append('comment', this.state.value);                
                formData.append('image_user', userData.auth.avatar);
                
                axios.post('http://localhost/laravel/laravel/public/api/blog/comment/' + this.props.idBlog, formData, config) 
                .then(res => {
                    this.setState({
                        commentSuccess: 'comment thanh cong'
                    })
                    
                    this.props.addComment(res.data.data)
                })
            }
        }
    }
 
    render () {
        return (
            <div className="replay-box">
                <div className="row">
                    <div className="col-sm-4">
                        <h2>Leave a replay</h2>
                        <form>
                            <div className="blank-arrow">
                                <label>Your Name</label>
                            </div>
                            <span>*</span>
                            <input type="text" placeholder="write your name..." />
                            <div className="blank-arrow">
                                <label>Email Address</label>
                            </div>
                            <span>*</span>
                            <input type="email" placeholder="your email address..." />
                            <div className="blank-arrow">
                                <label>Web Site</label>
                            </div>
                            <input type="email" placeholder="current city..." />
                        </form>
                    </div>
                    <div className="col-sm-8">
                        <div className="text-area">
                            <div className="blank-arrow">
                                <label>Your Name</label>
                            </div>
                            {this.state.commentSuccess}
                            <span>*</span>
                            <div>{this.state.errorLogin}</div>
                            <textarea value={this.state.value} onChange={this.handleChange} name="message" rows="11"></textarea>
                            <a onClick={this.handleSubmit} className="btn btn-primary" href="">post comment</a>
                        </div>
                    </div>  

                </div>
            </div>        
            
        )
    }
}

export default Comment


