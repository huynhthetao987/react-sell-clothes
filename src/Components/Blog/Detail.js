import React, { Component } from 'react';
import axios from 'axios';
import Comment from './Comment';
import ListComment from './ListComment';
import Rate from './Rate';

class Detail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            items: [],
            isLogin: '',
            errorLogin: '',
            comments: [],
            idReplay: ''
        }
        this.addComment = this.addComment.bind(this);
        this.getIdReplay = this.getIdReplay.bind(this);
    }
    
    componentDidMount() {
        var id = this.props.match.params.id;
        axios.get('http://localhost/laravel/laravel/public/api/blog/detail/' + id)
        .then(res => {
            const items = res.data.data;
            this.setState({
                items,
                comments: items.comment
            });
        })
    }

    addComment(data) {
        this.setState({
            comments: this.state.comments.concat(data)
        })
    }

    getIdReplay(id) {
        this.setState({idReplay: id})
    }

    render () {
        let item = this.state.items;
        var id = this.props.match.params.id;
        return (
            <div class="col-sm-9">
                <div class="blog-post-area">
                    <h2 class="title text-center">Latest From our Blog</h2>
                    <div class="single-blog-post">
                        <h3>{item['title']}</h3>
                        <div class="post-meta">
                            <ul>
                                <li><i class="fa fa-user"></i> Mac Doe</li>
                                <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                            </ul>
                            <span>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>
                            </span>
                        </div>  
                        <a href="">
                            <img src={"http://localhost/laravel/laravel/public/upload/Blog/image/" + item['image']} alt="" />
                        </a>  
                        <p>{item['description']}</p>
                        <div class="pager-area">
                            <ul class="pager pull-right">
                                <li><a href="#">Pre</a></li>
                                <li><a href="#">Next</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <Rate idBlog={id}/>
                <div class="socials-share">
                    <a href=""><img src="./html-frontend2/eshopper/images/blog/socials.png" alt="hello" /></a>
                </div> 
                <div class="media comments">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="images/blog/man-one.jpg" alt="" />
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">Annie Davis</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <div class="blog-socials">
                            <ul>
                                <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                <li><a href=""><i class="fa fa-dribbble"></i></a></li>
                                <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                            <a class="btn btn-primary" href="">Other Posts</a>
                        </div>  
                    </div> 
                </div>
                <ListComment listComment={this.state.comments} getIdReplay={this.getIdReplay} />
                <Comment idBlog={id} addComment={this.addComment} getIdReplay={this.state.idReplay} />       
            </div>         
        )
    }
}
export default Detail
