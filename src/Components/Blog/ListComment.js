import React, { Component } from 'react';

class ListComment extends Component {
    constructor(props) {
        super(props);
        this.handleId = this.handleId.bind(this);
    }

    handleId(e) {   
       this.props.getIdReplay(e.target.id);
    }

    fetchData() {
        let dataComment = this.props.listComment;
        if(dataComment) {
            if(dataComment.length > 0) {
                return dataComment.map((value, key) => {
                    if(value.id_comment == 0) {
                        return (
                            <>
                                <li key={key} className="media">
                                    <a className="pull-left" href="#">
                                        <img className="media-object" src={'http://localhost/laravel/laravel/public/upload/user/avatar/' + value.image_user} alt="" />
                                    </a>
                                    <div className="media-body">
                                        <ul className="sinlge-post-meta">
                                            <li><i className="fa fa-user" />{value.name_user}</li>
                                            <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                                            <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                                        </ul>
                                        <p>{value.comment}</p>
                                        <a onClick={this.handleId} className="btn btn-primary" id={value.id}><i className="fa fa-reply" />Replay</a>
                                    </div>
                                </li>  
                                {
                                    dataComment.map((value2, key2) => {
                                        if(value.id == value2.id_comment) {
                                            return (
                                                <li key={key2} className="media second-media">
                                                    <a className="pull-left" href="#">
                                                        <img className="media-object" src={'http://localhost/laravel/laravel/public/upload/user/avatar/' + value2.image_user} alt="" />
                                                    </a>
                                                    <div className="media-body">
                                                        <ul className="sinlge-post-meta">
                                                            <li><i className="fa fa-user" />{value2.name_user}</li>
                                                            <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                                                            <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                                                        </ul>
                                                        <p>{value2.comment}</p>
                                                        <a className="btn btn-primary"><i className="fa fa-reply" />Replay</a>
                                                    </div>
                                                </li>  
                                            )
                                        }
                                    })
                                }
                            </>
                        )
                    }
                })
            }
        }
    }   

    render() {
        return (
            <div className="response-area">
                <h2>3 RESPONSES</h2>
                <ul className="media-list">
                    {this.fetchData()}
                </ul>  
            </div>
        )
    }
}

export default ListComment


