import React , { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';

class MenuLeft extends Component {
    render() {
        return (
            <div className="col-sm-3">
                <div className="left-sidebar">
                    <h2>Category</h2>
                    <div className="panel-group category-products" id="accordian">
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h4 className="panel-title">
                                    <Link to='/account/member' data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                                        <span className="badge pull-right"><i className="fa fa-plus" /></span>
                                        ACCOUNT
                                    </Link>
                                </h4>
                            </div>
                        </div>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h4 className="panel-title">
                                    <Link to='/account/product/list' data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                                        <span className="badge pull-right"><i className="fa fa-plus" /></span>
                                        MY PRODUCT
                                    </Link>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default MenuLeft