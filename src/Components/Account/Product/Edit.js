import React, { Component } from 'react';
import axios from 'axios';

class Edit extends Component {
    constructor(props) {
        super(props)
        this.state = {
            brands: [],
            categories: [],
            images: '',
            name: '',
            price: '',
            brand: '',
            category: '',
            status: 1,
            sale: 0,
            file: [],
            newFile: [],
            company: '',
            detail: '',
            formError: {},
            msg: '',
            img_check: []
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleUserInputFile = this.handleUserInputFile.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.toggleChange = this.toggleChange.bind(this);
    }

    componentDidMount() {
        let id = this.props.match.params.id;
        let userData = JSON.parse(localStorage.getItem('userData'));
        let accessToken = userData.token;
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'multipart/form-data',
                'Accept': 'application/json'
            }
        }

        axios.get('http://localhost/laravel/laravel/public/api/user/product/' + id, config)
        .then(res => {
            const product = res.data.data;
            this.setState({
                name: product.name,
                price: product.price,
                category: product.id_category,
                brand: product.id_brand,
                status:product.status,
                sale: product.sale,
                company: product.company_profile,
                images: product.image,
                detail: product.detail
            })
        })

        axios.get('http://localhost/laravel/laravel/public/api/category-brand')
        .then(res => {
            const brands = res.data.brand;
            const categories = res.data.category;
            this.setState({brands, categories})
        })
    }   

    fetchDataCategory() {
        let categories = this.state.categories;
        return categories.map((value, key) => {
            return <option value={value.id}>{value.category}</option>
        })
    }

    fetchDataBrand() {
        let brands = this.state.brands;
        return brands.map((value, key) => {
            return <option value={value.id}>{value.brand}</option>
        })
    }
    
    fetchDataImage() {
        let userData = JSON.parse(localStorage.getItem('userData'));
        let id_user = userData.auth.id;

        let images = this.state.images;
        if(images.length > 0) {
            return images.map((value, key) => {
                return(
                    <li key={key}>
                        <img src={'http://localhost/laravel/laravel/public/upload/user/product/' + id_user + '/'  + value} />
                        <input type='checkbox' name="img_check" id={key} value={value} onChange={this.toggleChange} />
                    </li>
                )
            })
        }
    }

    handleChange(e) {
        const nameInput = e.target.name;
        const value = e.target.value;
        this.setState({
            [nameInput]: value
        })
    }

    handleUserInputFile(e) {
        this.setState({
            file: e.target.files
        })
    } 
    
    toggleChange(e) {
        let img_check = this.state.img_check;
        const id = e.target.id;
        const value = e.target.value;
        if(e.target.checked) {
            img_check[id]= value;
            this.setState({img_check})
        }
    }   

    handleSubmit(e) {
        e.preventDefault();
        
        let flag = true;
        let name = this.state.name;
        let price = this.state.price;
        let company = this.state.company;
        let detail = this.state.detail;
        let file = this.state.file;
        let errorSubmit = this.state.formError;
    
        if(!name) {
            flag = false;
            errorSubmit.name = 'vui long nhap name';
        } else {
            errorSubmit.name = '';
        }

        if(!price) {
            flag = false;
            errorSubmit.price = 'vui long nhap price';
        } else {
            errorSubmit.price = '';
        }

        if(file.length > 0) {
            Object.keys(file).map((key, index) => {
                var str = file[key]['name'];
                var res = str.split('.');
                var img = ['png', 'jpg', 'jpeg'];
                if(img.includes((res[1])) == false) {
                    flag = false;
                    errorSubmit.file = 'file khong hop le';
                } else {
                    errorSubmit.file = '';
                }
            })
        }

        if(!company) {
            flag = false;
            errorSubmit.company = 'vui long nhap company';
        } else {
            errorSubmit.company = '';
        } 

        if(!detail) {
            flag = false;
            errorSubmit.detail = 'vui long nhap detail';
        } else {
            errorSubmit.detail = '';
        }

        if(!flag) {
            this.setState({
                formError: errorSubmit
            })
        }
         else {
            let userData = JSON.parse(localStorage.getItem('userData'));
            let accessToken = userData.token;
            var id = this.props.match.params.id;
            let img_check = this.state.img_check;
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'multipart/form-data',
                    'Accept': 'application/json'
                }
            }

            const formData = new FormData();
            formData.append('name', this.state.name);
            formData.append('price', this.state.price);
            formData.append('category', this.state.category);
            formData.append('brand', this.state.brand);
            formData.append('company', this.state.company);
            formData.append('detail', this.state.detail);
            formData.append('status', this.state.status);
            formData.append('sale', this.state.sale);
            Object.keys(file).map((key, index) => {
                formData.append('file[]', file[key]);
            })
            if(img_check.length > 0) {
                img_check.map((value, key) => {
                    formData.append('avatarCheckBox[]', value);
                })
            } else {
                formData.append('avatarCheckbox[]', '')
            }
            
            axios.post('http://localhost/laravel/laravel/public/api/user/edit-product/' + id, formData, config)
            .then(res => {
                console.log(res);
                if(res.data.message) {
                    this.setState({
                        msg: res.data.message
                    })
                } else {
                    this.setState({
                        msg: 'edit thanh cong'
                    })
                }
            })
        }
    }

    renderError() {
        let formError = this.state.formError;
        return (
            <div>
                {Object.keys(formError).map((key, index) => {
                    if(formError[key].length > 0) {
                        return (
                            <p key={index}>{formError[key]}</p>
                        )
                    } else {
                        return '';
                    }
                })}
            </div>
        )
    }

    render() {
        return(
            <div className="col-sm-8">
                <div className="signup-form">
                    <div>{this.renderError()}</div>
                    {this.state.msg}
                    <h2>Create Product!</h2>
                    <form onSubmit={this.handleSubmit}>
                        <input type="text" name="name" value={this.state.name} onChange={this.handleChange} />
                        <input type="text" name="price" value={this.state.price} onChange={this.handleChange} />
                        <select name="category" onChange={this.handleChange}>
                            <option value={this.state.category}>{'Category' + this.state.category}</option> 
                            {this.fetchDataCategory()}
                        </select>
                        <select name="brand" onChange={this.handleChange}>
                            <option value={this.state.brand}>{'Brand' + this.state.brand}</option>
                            {this.fetchDataBrand()}
                        </select>
                        <select name="status" onChange={this.handleChange}>
                            <option>Please choose status</option>
                            <option value="1">new</option>
                            <option value="2">sale</option>
                        </select>
                        <input name="sale" type="number" value={this.state.sale} onChange={this.handleChange} />
                        <input type="text" name="company" value={this.state.company} onChange={this.handleChange} />
                        <input type="file" name="file" onChange={this.handleUserInputFile} multiple/>
                        <ul>
                            {this.fetchDataImage()}
                        </ul>
                        <input id="detail" name="detail" value={this.state.detail} onChange={this.handleChange} />
                        <button type="submit" class="btn btn-default btn-add">Signup</button>
                    </form> 
                </div>
            </div>        
        )
    }        
}

export default Edit