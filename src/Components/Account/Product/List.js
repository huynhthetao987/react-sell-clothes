import React, { Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';

class List extends Component {
    constructor(props) {
        super(props)
        this.state = {
            products: {}
        }
    }

    componentDidMount() {
        let userData = JSON.parse(localStorage.getItem('userData'));
        let accessToken = userData.token;
        let config = {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'multipart/form-data',
                'Accept': 'application/json'
            }
        }

        Axios.get('http://localhost/laravel/laravel/public/api/user/my-product', config)
        .then(res => {
            this.setState({
                products: res.data.data
            })
        })
    }
    
    fetchData() {
        let products = this.state.products;
        return Object.keys(products).map((key,index) => {
            const images = JSON.parse(products[key].image);
            return (
                <tr key={index} className="cart_body">
                    <td className="id">{products[key].id}</td>
                    <td className="name">{products[key].name}</td>
                    <td className="image">
                        <img src={'http://localhost/laravel/laravel/public/upload/user/product/' + products[key].id_user + "/" + images[0]} />
                    </td>
                    <td className="price">{products[key].price}</td>
                    <td>
                        <Link to={'/account/product/edit/' +  products[key]['id']}><span class="glyphicon glyphicon-pencil"></span></Link>
                        <Link to={'/account/product/delete/' + products[key]['id']}><span class="glyphicon glyphicon-remove-sign"></span></Link>
                    </td>
                </tr>
            )
        })
    }

    render() {
        return (
            <>
                <section id="cart_items">
                    <div className="container">
                        <div className="table-responsive cart_info">
                            <table className="table table-condensed">
                                <thead>
                                    <tr className="cart_menu">
                                        <td className="id">Id</td>
                                        <td className="name">Name</td>
                                        <td className="image">Image</td>
                                        <td className="price">Price</td>
                                        <td className="action">Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.fetchData()}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section> 
                <Link to='/account/product/add'><input className="add" type="button" value="Add New" /></Link>
            </>
        )
    }
}

export default List