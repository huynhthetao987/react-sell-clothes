import React, { Component } from 'react';
import Axios from 'axios';

class Add extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            price: '',
            brands: [],
            brand: '',
            categories: [],
            category: '',
            status: 1,
            sale: 0,
            file: '',
            company: '',
            detail: '',
            formError: {},
            msg: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleUserInputFile = this.handleUserInputFile.bind(this);
    }

    componentDidMount() {
        Axios.get('http://localhost/laravel/laravel/public/api/category-brand')
        .then(res => {
            const brands = res.data.brand;
            const categories = res.data.category;
            this.setState({brands, categories})
        })
    }

    fetchDataCategory() {
        let categories = this.state.categories;
        return categories.map((value, key) => {
            return <option name="category" value={value.id} onChange={this.handleChange}>{value.category}</option>
        })
    }

    fetchDataBrand() {
        let brands = this.state.brands;
        return brands.map((value, key) => {
            return <option name="brand" value={value.id} onChange={this.handleChange}>{value.brand}</option>
        })
    }

    handleChange(e) {
        const nameInput = e.target.name;
        const value = e.target.value;
        this.setState({
            [nameInput]: value
        })
    }

    handleUserInputFile(e) {
        this.setState({ 
            file: e.target.files
        })
    }

    handleSubmit(e) {
        e.preventDefault();

        let flag = true;
        let name = this.state.name;
        let price = this.state.price;
        let category = this.state.category;
        let brand = this.state.brand;
        let company = this.state.company;
        let detail = this.state.detail;
        let file = this.state.file;
        let errorSubmit = this.state.formError;
    
        if(!name) {
            flag = false;
            errorSubmit.name = 'vui long nhap name';
        } else {
            errorSubmit.name = '';
        }

        if(!price) {
            flag = false;
            errorSubmit.price = 'vui long nhap price';
        } else {
            errorSubmit.price = '';
        }

        if(!category) {
            flag = false;
            errorSubmit.category = 'vui long chon category';
        } else {
            errorSubmit.category = '';
        }

        if(!brand) {
            flag = false;
            errorSubmit.brand = 'vui long chon brand';
        } else {
            errorSubmit.brand = '';
        }

        if(file.length > 0) {
            if(file.length > 3) {
                flag = false;
                errorSubmit.file = 'chi chon toi da 3 file';
            } else {
                Object.keys(file).map((key, index) => {
                    var str = file[key]['name'];
                    var res = str.split('.');
                    var img = ['png', 'jpg', 'jpeg'];
                    if(img.includes((res[1])) == false) {
                        flag = false;
                        errorSubmit.file = 'file khong hop le';
                    } else {
                        errorSubmit.file = '';
                    }
                })
            }
        } else {
            errorSubmit.file = 'vui long chon file';
        }

        if(!company) {
            flag = false;
            errorSubmit.company = 'vui long nhap company';
        } else {
            errorSubmit.company = '';
        } 

        if(!detail) {
            flag = false;
            errorSubmit.detail = 'vui long nhap detail';
        } else {
            errorSubmit.detail = '';
        }

        if(!flag) {
            this.setState({
                formError: errorSubmit
            })
        } else {
            let userData = JSON.parse(localStorage.getItem('userData'));
            let accessToken = userData.token;
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'multipart/form-data',
                    'Accept': 'application/json'
                }
            }

            const formData = new FormData();
            formData.append('name', this.state.name);
            formData.append('price', this.state.price);
            formData.append('category', this.state.category);
            formData.append('brand', this.state.brand);
            formData.append('company', this.state.company);
            formData.append('detail', this.state.detail);
            formData.append('status', this.state.status);
            formData.append('sale', this.state.sale);
            Object.keys(file).map((key, index) => {
                formData.append('file[]', file[key]);
            })

            Axios.post('http://localhost/laravel/laravel/public/api/user/add-product', formData, config)
            .then(res => {
                this.setState({
                    msg: 'add thanh cong'
                })
            })
        }
    }

    renderError() {
        let formError = this.state.formError;
        return (
            <div>
                {Object.keys(formError).map((key, index) => {
                    if(formError[key].length > 0) {
                        return (
                            <p key={index}>{formError[key]}</p>
                        )
                    } else {
                        return '';
                    }
                })}
            </div>
        )
    }

    renderSale() {
        let status = this.state.status;
        if(status == "2") {
            return (
                <input id="sale" name="sale" type="number" onChange={this.handleChange} />
            )
        }
    }

    render() {
        console.log(this.state)
        return (
            <div className="col-sm-8">
                <div className="signup-form">
                    <div>{this.renderError()}</div>
                    {this.state.msg}
                    <h2>Create Product!</h2>
                    <form onSubmit={this.handleSubmit}>
                        <input type="text" name="name" placeholder="Name" onChange={this.handleChange} />
                        <input type="text" name="price" placeholder="Price" onChange={this.handleChange} />
                        <select name="category" onChange={this.handleChange}>
                            <option value="">Please choose category</option>
                            {this.fetchDataCategory()}
                        </select>
                        <select name="brand" onChange={this.handleChange}>
                            <option value="">Please choose brand</option>
                            {this.fetchDataBrand()}
                        </select>
                        <select name="status" onChange={this.handleChange}>
                            <option value="1">new</option>
                            <option value="2">sale</option>
                        </select>
                        {this.renderSale()}
                        <input type="text" name="company" placeholder="Company profile" onChange={this.handleChange} />
                        <input type="file" name="file" onChange={this.handleUserInputFile} multiple/>
                        <input id="detail" name="detail" placeholder="Detail" onChange={this.handleChange} />
                        <button type="submit" class="btn btn-default btn-add">Signup</button>
                    </form>
                </div>
            </div>
        )
    }
}

export default Add