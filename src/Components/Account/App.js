import React , {Component} from 'react';
import { withRouter } from 'react-router-dom';
import MenuLeft from './MenuLeft';

class App extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        let pathname = this.props.location.pathname;
        return (
            <div>
                {pathname.includes("account") ? <MenuLeft /> : ""}
                {this.props.children}
            </div>
        )
    }
}
 export default withRouter(App);