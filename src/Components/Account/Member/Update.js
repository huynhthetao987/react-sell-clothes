import Axios from 'axios';
import React, { Component } from 'react';

class Update extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            password: '',
            phone: '',
            address: '',
            avatar: '',
            id: '',
            file: '',
            formError: {},
            msg: ''
        }
        this.handleInput = this.handleInput.bind(this);
        this.handleUserInputFile = this.handleUserInputFile.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        let convertUserData = localStorage.getItem('userData');
        if(convertUserData) {
            let userData = JSON.parse(convertUserData)
            this.setState({
                name: userData.auth.name,
                email: userData.auth.email,
                phone: userData.auth.phone,
                address: userData.auth.address,
                id: userData.auth.id
            })
        }
    }

    handleInput(e) {
        const nameInput = e.target.name;
        const value = e.target.value;
        this.setState({
            [nameInput]: value
        })
    }

    handleUserInputFile (e){
        const file = e.target.files;
        let reader = new FileReader();
        reader.onload = (e) => {
            this.setState({
                avatar: e.target.result,        
                file: file[0]                 
            })
        };
        reader.readAsDataURL(file[0]);
    }	

    handleSubmit(e) {
        e.preventDefault();

        let flag = true;
        let name = this.state.name;
        let email = this.state.email;
        let password = this.state.password;
        let phone = this.state.phone;
        let address = this.state.address;
        let avatar = this.state.avatar;
        let errorSubmit = this.state.formError;
        let file = this.state.file;

        if(!name) {
            flag = false;
            errorSubmit.name = "vui long nhap name";
        } else {
            errorSubmit.name = '';
        }

        if(!phone) {
            flag = false;
            errorSubmit.phone = "vui long nhap phone";
        } else {
            errorSubmit.phone = '';
        }
        
        if(!address) {
            flag = false;
            errorSubmit.address = "vui long nhap address";
        } else {
            errorSubmit.address = '';
        }

        if(file['name']) {
            var str = (file['name']);
            var res = str.split(".");
            var img = ['png', 'jpg', 'jpeg'];
            if(img.includes((res[1])) == true) {
                avatar = file
            } 
            errorSubmit.avatar = ''; 
        } else {
            flag = false;
            errorSubmit.avatar = "vui long chon avatar";
        }

        if(!flag) {
            this.setState({
                formError: errorSubmit
            })
        } else {
            let userData = JSON.parse(localStorage.getItem('userData'))
            let accessToken = userData.token;
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            }

            const formData = new FormData();
            formData.append('name', this.state.name);
            formData.append('email', this.state.email);
            formData.append('password', this.state.password);
            formData.append('phone', this.state.phone);
            formData.append('address', this.state.address);
            formData.append('avatar', this.state.avatar);
            formData.append('level', 0);

            Axios.post('http://localhost/laravel/laravel/public/api/user/update/' + this.state.id, formData, config)
            .then(res => {
                console.log(res)
                this.setState({
                    msg: 'update thanh cong'
                })
                const user = {
                    auth: {
                        id: this.state.id,
                        name: this.state.name,
                        email: this.state.email,
                        phone: this.state.phone,
                        address: this.state.address,
                        avatar: this.state.avatar,
                        level: 0
                    },
                    token: accessToken
                }
                localStorage.setItem('userData', JSON.stringify(user))
            })
        }
    }

    renderError() {
        let formError = this.state.formError;
        return (
            <div>
            {Object.keys(formError).map((key, index) => {
                if(formError[key].length > 0) {
                    return (
                        <div key={index}>{formError[key]}</div>
                    )
                }
            })}
            </div>
        )
    }

    render() {
        return(
            <section id="form"> 
                <div className="col-sm-6">
                    <div className="update-form signup-form" >
                        <h2>User update!</h2>
                        <p>{this.state.msg}</p>
                        <p>{this.renderError()}</p>
                        <form onSubmit={this.handleSubmit}>
                            <input type="text" name="name" value={this.state.name} onChange={this.handleInput} />
                            <input type="email" name="email" value={this.state.email} />
                            <input type="password" name="password" value={this.state.password} onChange={this.handleInput}/>
                            <input type="number" name="phone" value={this.state.phone} onChange={this.handleInput} />
                            <input type="text" name="address" value={this.state.address} onChange={this.handleInput} />
                            <input type="file" name="avatar" onChange={this.handleUserInputFile} />
                            <button type="submit" class="btn btn-default">Update</button>
                        </form>
                    </div>
                </div>
            </section>
        )
    }
}

export default Update