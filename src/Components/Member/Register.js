import axios from 'axios';
import React, { Component } from 'react';

class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            password: '',
            phone: '',
            address: '',
            avatar: '',
            formError: {},
            file: '',
            msg: ''
        }
        this.handleInput = this.handleInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleUserInputFile = this.handleUserInputFile.bind(this);
    }

    handleInput(e) {
        const nameInput = e.target.name;
        const value = e.target.value
        this.setState({
            [nameInput]: value
        })
    }

    handleUserInputFile (e){
        const file = e.target.files;

        // send file to api server
        let reader = new FileReader();
        reader.onload = (e) => {
            this.setState({
                avatar: e.target.result,        //cai nay de gui qua api
                file: file[0]                   //cai nay de toan bo thong file upload vao file de xuong check validate
            })
        };
        reader.readAsDataURL(file[0]);
    }				
                                   
    handleSubmit(e) {
        e.preventDefault();
        
        let flag = true;
        let name = this.state.name;
        let email = this.state.email;
        let password = this.state.password;
        let phone = this.state.phone;
        let address = this.state.address;
        let avatar = this.state.avatar;
        let file = this.state.file;
        let errorSubmit = this.state.formError;

        if(!name) {
            flag = false;
            errorSubmit.name = "vui long nhap name";
        } else {
            errorSubmit.name = "";
        }

        if(!email) {
            flag = false;
            errorSubmit.email = "vui long nhap email";
        } else {
            errorSubmit.email = '';
        }

        if(!password) {
            flag = false;
            errorSubmit.password = "vui long nhap password";
        } else {
            errorSubmit.password = '';
        }

        if(!phone) {
            flag = false;
            errorSubmit.phone = "vui long nhap phone";
        } else {
            errorSubmit.phone = "";
        }

        if(!address) {
            flag = false;
            errorSubmit.address = "vui long nhap address";
        } else {
            errorSubmit.address = "";
        }
        
        if(file['name']) {
            var str = (file['name']);
            var res = str.split(".");
            var img = ['png', 'jpg', 'jpeg'];
            if(img.includes((res[1])) == true) {
                avatar = file
            } 
            errorSubmit.avatar = '';
        } else {
            flag = false;
            errorSubmit.avatar = "vui long chon avatar";
        }

        if(!flag) {
            this.setState ({
                formError: errorSubmit
            })
        } else {
            const user = {
                name: this.state.name,
                email: this.state.email,
                password: this.state.password,
                phone: this.state.phone,
                address: this.state.address,
                avatar: this.state.avatar,
                level: 0
            };
           axios.post('http://localhost/laravel/laravel/public/api/register', user)
           .then(res => {
               console.log(res)
                const errors = res.data.errors
                if(errors) {
                    this.setState ({
                        formError: errors
                    })
                } else {
                    this.setState ({
                        msg: "ban da dk thanh cong"
                    })
                    // this.props.history.push('/login')
                }
           })
        }
    }

    // renderError() {
    //     let formError = this.state.formError;
    //     return (
    //         <div>
    //             {Object.keys(formError).map((key, index) => {
    //                 if(formError[key].length > 0) {
    //                     return (
    //                         <p key={index}>{formError[key]}</p>
    //                     )
    //                 } 
    //             })}
    //         </div>
    //     )
    // }

    render() {
        let formError = this.state.formError;
        return (
            <div className="col-sm-6">
                {/* {this.renderError()} */}
                <div className="signup-form">
                    {this.state.msg}
                    <h2>New User Signup!</h2>
                    <form onSubmit={this.handleSubmit}>
                        <p>{formError.name}</p>
                        <input type="text" name="name" placeholder="Name" onChange={this.handleInput} />
                        <p>{formError.email}</p>
                        <input type="email" name="email" placeholder="Email Address" onChange={this.handleInput} />
                        <p>{formError.password}</p>
                        <input type="password" name="password" placeholder="Password" onChange={this.handleInput} />
                        <p>{formError.phone}</p>
                        <input type="number" name="phone" placeholder="Phone" onChange={this.handleInput} />
                        <p>{formError.address}</p>
                        <input type="text" name="address" placeholder="Address" onChange={this.handleInput} />
                        <p>{formError.avatar}</p>
                        <input type="file" placeholder="Avatar" name="avatar" onChange={this.handleUserInputFile}/>
                        <button type="submit" class="btn btn-default">Signup</button>
                    </form>
                </div>
            </div>          
        )
    }
}

export default Register





// 2bien:
//       - bien hinh da ma hoa =>api
//       - bien de lay all thong tin file up len. de kiem tra co pai hinh anh hay k?

//       avatar tra 1 mang:[
//         name: abc.png
//         size: 10000
//         type,
//         ...CaretPosition.
//       ]

//       nhung file co duoi file: [png, jpg, jpep]
//       lay duôi file cua file up len,bang cach lay name ra, cat để lay duoi file (png)
//       kiem tra duoi file nay co nam trong mang k, neu k co thi show loi:
//       vui long upload file image


// - tim cach lay duoi file.
            // - tao 1 mang chua duoi hinh anh: ['png', 'jpg', 'jpeg']
            // - kiem tra duoi file co trong mang tren hay k?
            //   + k co thi show loi