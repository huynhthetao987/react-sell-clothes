import React, { Component } from 'react';
import axios from 'axios';
import Register from './Register';

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            emailError: '',
            passError: '',
            errorLogin: '',
            msg: ''
        }
        this.handleEmail = this.handleEmail.bind(this);
        this.handlePass = this.handlePass.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleEmail(e) {
        this.setState({
            email: e.target.value
        })
    }

    handlePass(e) {
        this.setState({
            password: e.target.value
        })
    }

    handleSubmit(e) {
        e.preventDefault();

        let flag = true;
        let email = this.state.email;
        let password = this.state.password;
        if(!email) {
            flag = false;
            this.setState ({
                emailError: "vui long nhap email"
            })
        } else {
            this.setState ({
                emailError: ""
            })
        }

        if(!password) {
            flag = false;
            this.setState ({
                passError: "vui long nhap password"
            })
        } else {
            this.setState ({
                passError: ""
            })
        }
        
        if(flag) {
            const user = {
                email: this.state.email,
                password: this.state.password,
                level: 0
            }
            axios.post('http://localhost/laravel/laravel/public/api/login', user)
            .then(res => {
                console.log(res)
                const errors = res.data.errors
                if(errors) {
                    this.setState ({
                       errorLogin: errors.errors
                    })
                } else {
                    this.setState ({
                        msg: 'dang nhap thanh cong'
                     })
                     
                     this.setState({
                         errorLogin: ''
                     })

                    let userData = {
                       auth: res.data.Auth,
                       token: res.data.success.token
                    }
                    localStorage.setItem('userData', JSON.stringify(userData));
                   
                   this.props.history.push('/home')
                }
            })  
        }
    }
    
    render() {
        return (
            <section id="form">
                <div className="container">
                    <div className="row">
                        <div class="col-sm-4 col-sm-offset-1">
                            <div class="login-form">
                                <h2>Login to your account</h2>
                                <div>{this.state.errorLogin}</div>
                                <div>{this.state.msg}</div>
                                <form onSubmit={this.handleSubmit}>
                                    <p>{this.state.emailError}</p>
                                    <input type="email" name="email" placeholder="Email Address" onChange={this.handleEmail} value={this.state.email} />
                                    <p>{this.state.passError}</p>
                                    <input type="password" name="password" placeholder="Password" onChange={this.handlePass} value={this.state.password} />
                                    <span>
                                        <input type="checkbox" class="checkbox" /> 
                                        Keep me signed in
                                    </span>
                                    <button type="submit" class="btn btn-default">Login</button>
                                </form>
                            </div>
                        </div>   
                        <div class="col-sm-1">
                            <h2 class="or">OR</h2>
                        </div> 
                        <Register />      
                    </div>
                </div>
            </section>   
        )
    }
}

export default Login

